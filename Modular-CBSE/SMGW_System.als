open cbseFramework
module SMGW_System

// Simple SMGW case study for defining simple component action framework

// Gateway ==========================================================
one sig CC_Gateway_TOE extends CompositeComponent {}{
	contains = AC_Gateway_Comms + AC_Gateway_TSF + AC_Security_Module
}

// Component facilitating Gateway to x communication
one sig AC_Gateway_Comms extends AtomicComponent {}{
	uses = IN_GW_LMN + OUT_GW_LMN +
		  IN_Comms_TSF + OUT_Comms_TSF +
		  IN_GW_WAN + OUT_GW_WAN
}
one sig IN_GW_LMN extends InPort {}{
	realizes = IF_MTR2GW
}
one sig OUT_GW_LMN extends OutPort {}{
	realizes = IF_GW2MTR
}
one sig IN_Comms_TSF extends InPort {}{
	realizes = IF_GW2Comms
}
one sig OUT_Comms_TSF extends OutPort {}{
	realizes = IF_Comms2GW
}
one sig IN_GW_WAN extends InPort {}{
	realizes = IF_ExtEnt2GW + IF_Admin2GW
}
one sig OUT_GW_WAN extends OutPort {}{
	realizes = IF_GW2ExtEnt + IF_GW2Admin
}

// Security Functionality of the gateway (meter processing,
// identification, authentication, ) Should be split up further
one sig AC_Gateway_TSF extends AtomicComponent{
	current_processing_profiles: set ProcessingProfile,
	past_processing_profiles: set ProcessingProfile,
	var processed_meter_data: set ProcessedMeterData
}{
	uses = IN_TSF_Comms + OUT_TSF_Comms +
			IN_TSF_SecMod + OUT_TSF_SecMod
	// TODO move this to processed meter data restrictions
	// All processed meter data has unique processed meterdata related to one MeterData
	always all disj p1,p2:processed_meter_data | p1.p_meter_data != p2.p_meter_data
}
one sig IN_TSF_Comms extends InPort {}{
	realizes = IF_Comms2GW
}
one sig OUT_TSF_Comms extends OutPort {}{
	realizes = IF_GW2Comms
}
one sig IN_TSF_SecMod extends InPort {}{
	realizes = IF_SM2GW
}
one sig OUT_TSF_SecMod extends OutPort {}{
	realizes = IF_GW2SM
}

// The Security Module (SecMod) dealing with all cryptographic functions and secret
// storage
one sig AC_Security_Module extends AtomicComponent {
	shared_creds: set Credentials
}{
	uses = IN_SecMod_TSF + OUT_SecMod_TSF

	// State restrictions
	all a: PreAuthorized_Entity | a.shared_cred in shared_creds // All shared_secrets are stored in the SecMod
}
one sig IN_SecMod_TSF extends InPort {}{
	realizes = IF_GW2SM
}
one sig OUT_SecMod_TSF extends OutPort {}{
	realizes = IF_SM2GW
}

one sig CON_TSF_Comms extends Connector {}{
	connects = IN_Comms_TSF + OUT_Comms_TSF + 
				IN_TSF_Comms + OUT_TSF_Comms
}

one sig CON_TSF_SecMod extends Connector {}{
	connects = IN_SecMod_TSF + OUT_SecMod_TSF + 
				IN_TSF_SecMod + OUT_TSF_SecMod
}
// ==================================================================

// LMN Network containing all meters ================================
one sig CC_LMN extends CompositeComponent {}{
	contains = AC_Meter
}

some sig AC_Meter extends PreAuthorized_Entity {
	var mdata: set MeterData
}{
	#uses = 2
	one (uses & IN_Meter)
	one (uses & OUT_Meter)
}
some sig IN_Meter extends InPort {}{
	realizes = IF_GW2MTR
}
some sig OUT_Meter extends OutPort {}{
	realizes = IF_MTR2GW
}

one sig CON_LMN_NET extends Connector {}{
	connects = IN_Meter + OUT_Meter +
				IN_GW_LMN + OUT_GW_LMN
}
// ==================================================================

// WAN Network Components  ==========================================
one sig CC_WAN extends CompositeComponent {}{
	contains = AC_Ext_Ent + AC_Admin
}

some sig AC_Ext_Ent extends PreAuthorized_Entity {
	var p_mdata_storage: set ProcessedMeterData
}{
	#uses = 2
	one (uses & IN_Ext_Ent)
	one (uses & OUT_Ext_Ent)
}
some sig IN_Ext_Ent extends InPort {}{
	realizes = IF_GW2ExtEnt
}
some sig OUT_Ext_Ent extends OutPort {}{
	realizes = IF_ExtEnt2GW
}

one sig AC_Admin extends PreAuthorized_Entity {}{
	uses = IN_Admin + OUT_Admin
}
one sig IN_Admin extends InPort {}{
	realizes = IF_GW2Admin
}
one sig OUT_Admin extends OutPort {}{
	realizes = IF_Admin2GW
}

one sig CON_WAN_NET extends Connector {}{
	connects = IN_Ext_Ent + OUT_Ext_Ent +
				IN_Admin + OUT_Admin +
				IN_GW_WAN + OUT_GW_WAN
}
// ==================================================================

// A connector that ensures all messages sent over its service are delivered (ex, TCP)
abstract sig ReliableConnector extends Connector {}
fact ReliableDelivery {
	always all rc: ReliableConnector | all m:Message | (m in rc.buffer => eventually m.receivedOn[])
	always all rc: ReliableConnector | all m:Message | m in (rc.connects & OutPort).sent => after m in rc.buffer // no drop on this connector
}

// Abstract Components
abstract sig PreAuthorized_Entity extends AtomicComponent {
	shared_cred: disj one Credentials
}

// Logical Interfaces ===============================================

// IF_LMN
// IF_GW_MTR (Meter)
one sig IF_MTR2GW extends Interface {}{
	operations = OP_send_raw_data
}
one sig OP_send_raw_data extends Operation {}{
	params = P_RawMeterData
}
one sig P_RawMeterData extends Parameter {}{
	passes = MeterData
}
one sig IF_GW2MTR extends Interface {}{
	operations = OP_max_data_size
}
one sig OP_max_data_size extends Operation {}{
	no params
}

//IF_GW_WAN (WAN components)
//	IF_GW_ExtEnt (Extneral Entity)
one sig IF_GW2ExtEnt extends Interface {}{
	operations = OP_receive_p_data
}
one sig OP_receive_p_data extends Operation {}{
	params = P_ExtEntPData
}
one sig P_ExtEntPData extends Parameter {}{
	passes = ProcessedMeterData
}
one sig IF_ExtEnt2GW extends Interface {}{
	operations = none
}
//	IF_GW_Admin (Admin)
one sig IF_GW2Admin extends Interface {}{
	operations = none
}
one sig IF_Admin2GW extends Interface {}{
	operations = none
}

// Internal Gateway Interfaces
// IF_GW_SM (Security Module)
one sig IF_SM2GW extends Interface {}{
	operations = OP_auth_meter_reply
}
one sig OP_auth_meter_reply extends Operation {}{
	params = P_MeterAuth_reply + P_MeterAuth_data
}
one sig P_MeterAuth_reply extends Parameter {}{
	passes = Validity
}
one sig P_MeterAuth_data extends Parameter {}{
	passes = MeterData
}
one sig IF_GW2SM extends Interface {}{
	operations = OP_auth_meter
}
one sig OP_auth_meter extends Operation {}{
	params = P_MeterAuth
}
one sig P_MeterAuth extends Parameter {}{
	passes = MeterData
}

// IF_GW_Comms (Communication to TSF)
one sig IF_Comms2GW extends Interface {}{
	operations = OP_process_mdata
}
one sig OP_process_mdata extends Operation {}{
	params = P_MeterDataComms
}
one sig P_MeterDataComms extends Parameter {}{
	passes = MeterData
}
one sig IF_GW2Comms extends Interface {}{
	operations = OP_send_p_mdata
}
one sig OP_send_p_mdata extends Operation {}{
	params = P_ProcessedMeterData
}
one sig P_ProcessedMeterData extends Parameter {}{
	passes = ProcessedMeterData
}

var sig OpCall extends OperationCall {}

// ==================================================================

// Component Behaviour ==============================================
// Need predicates to describe a components valid actions and a fact to restrict the actions possible for a component
// The action predicates also restrict the fields of a component's state variables

// AC_Meter ===============================================
fact AC_Meter_trans {
	always all m:AC_Meter {
		(m.generateMeterData or (some md:MeterData | m.sendMeterData[md]) or m.doNothing)
		all md:m.mdata | eventually m.sendMeterData[md]
		// Output should be messages that component sends in action
		some md:MeterData | some m.output[].sent => m.sendMeterData[md]
	}
}
pred AC_Meter.generateMeterData[] {
	after one md:MeterData {
		before md not in MeterData
	      before this.mdata' = this.mdata + md
		md.generated_by = this
	}
}
pred AC_Meter.sendMeterData[md:MeterData] {
	// Check meter data origin
	md.generated_by = this
	md in this.mdata
	this.send[AC_Gateway_Comms,OP_send_raw_data]
	// Filter by OpCalls sent by this meter
	(arguments.md.P_RawMeterData & this.output[].sent).sentOn[]
	one this.output[].sent // Send one message
	this.mdata' = this.mdata - md
}
pred AC_Meter.doNothing[] {
	this.mdata' = this.mdata
}

// Gateway Comms ==========================================
fact AC_Gateway_Comms_trans {
	// The Comms component should be stateless as it acts as a communication unit that passes messages to and from gateway core
	always AC_Gateway_Comms.Comms_In_Guarantee
}
pred AC_Gateway_Comms.Comms_In_Guarantee {
	all i:this.input[].received {
		(i.operation = OP_send_raw_data and 
			this.forwardMeterData[i.arguments[P_RawMeterData]]) or
		(i.operation = OP_send_p_mdata and 
			this.forwardProcessedMeterData[i.arguments[P_ProcessedMeterData]])
	}
}
pred AC_Gateway_Comms.forwardMeterData[md:MeterData] {
	after this.send[AC_Gateway_TSF,OP_process_mdata]
	md in this.output[].sent'.arguments'[P_MeterDataComms]
}
pred AC_Gateway_Comms.forwardProcessedMeterData[pmd:ProcessedMeterData] {
	//after (this.send[(pmd.dest),OP_receive_p_data])
	one oc:this.output[].sent' {
		pmd.dest in oc.to'.user[]
		pmd in oc.arguments'[P_ExtEntPData]
	}
}

// Gateway TSF ============================================
fact AC_Gateway_TSF_trans {
	always AC_Gateway_TSF.TSF_In_Guarantee
}
pred AC_Gateway_TSF.TSF_In_Guarantee {
	(some this.input[].received => (all i:this.input[].received {
		(i.operation = OP_process_mdata and 
			this.validateMeterData[i.arguments[P_MeterDataComms]]) or
		(i.operation = OP_auth_meter_reply and 
			(i.arguments[P_MeterAuth_reply] = VALID =>
				this.processMeterData[i.arguments[P_MeterAuth_data]] else
				this.doNothing))
	}) else
	(some this.processed_meter_data =>
		(one pmd:this.processed_meter_data | this.sendProcessedMeterData[pmd]) else
	this.doNothing))
}
pred AC_Gateway_TSF.validateMeterData[md:MeterData] {
	after this.send[AC_Security_Module, OP_auth_meter] and
	md in this.output[].sent'.arguments'[P_MeterAuth]
	this.processed_meter_data' = this.processed_meter_data
}
pred AC_Gateway_TSF.processMeterData[md:MeterData] {
	one pmd:ProcessedMeterData {
		pmd.p_meter_data = md
		pmd.dest = this.getMeterExtAddr[md.generated_by]
		this.processed_meter_data' = this.processed_meter_data + pmd
		before pmd not in ProcessedMeterData
	}
}
pred AC_Gateway_TSF.sendProcessedMeterData[pmd:ProcessedMeterData] {
	pmd in this.processed_meter_data
	after this.send[AC_Gateway_Comms, OP_send_p_mdata]
	pmd in this.output[].sent'.arguments'[P_ProcessedMeterData]
	this.processed_meter_data' = this.processed_meter_data - pmd
}
fun AC_Gateway_TSF.getMeterExtAddr[m:AC_Meter]: one AC_Ext_Ent {
	{ ee:AC_Ext_Ent | one cpp:this.current_processing_profiles | m in cpp.meters and cpp.addr = ee }
}
pred AC_Gateway_TSF.doNothing {
	this.processed_meter_data' = this.processed_meter_data
}

// Security Module ========================================
fact AC_Security_Module_trans {
	always AC_Security_Module.SecMod_In_Guarantee
}
pred AC_Security_Module.SecMod_In_Guarantee {
	all i:this.input[].received {
		i.operation = OP_auth_meter and this.validateMeter[i.arguments[P_MeterAuth]]
	}
}
pred AC_Security_Module.validateMeter[md:MeterData] {
	md in this.output[].sent'.arguments'[P_MeterAuth_data] and {
			md.generated_by in this.shared_creds.~shared_cred => 
				this.output[].sent'.arguments'[P_MeterAuth_reply] = VALID else 
				this.output[].sent'.arguments'[P_MeterAuth_reply] = INVALID
	} and after this.send[AC_Gateway_TSF,OP_auth_meter_reply]
}

// ==================================================================

// External Entity ==================================================
fact AC_Ext_Ent_init {
	no AC_Ext_Ent.p_mdata_storage
}
fact AC_Ext_Ent_trans {
	always all ee:AC_Ext_Ent | ee.ExtEnt_In_Guarantee
}
pred AC_Ext_Ent.ExtEnt_In_Guarantee {
	some this.input[].received=> (all i:this.input[].received {
		i.operation = OP_receive_p_data and this.savePMD[i.arguments[P_ExtEntPData]]
	}) else this.doNothing
}
pred AC_Ext_Ent.savePMD[pmd:ProcessedMeterData] {
	pmd.dest = this => (this.p_mdata_storage' = this.p_mdata_storage + pmd) else
						this.doNothing
}
pred AC_Ext_Ent.doNothing {
	this.p_mdata_storage' = this.p_mdata_storage
}
// ==================================================================

// Types ============================================================

abstract var sig MeterData extends Variable {
	var generated_by: one AC_Meter
}{
	// The generated_by field stays the same until the meter data no longer exists
	generated_by' = generated_by or after this not in MeterData
	once one mdata.this // Originates from a meter 
}
var sig ConsumptionData extends MeterData {}

abstract sig Validity extends Constant {}
one sig VALID extends Validity {}
one sig INVALID extends Validity {}

sig Credentials extends Constant {}

sig ProcessingProfile extends Constant {
	user: one User, // The user who owns the profile
	meters: some AC_Meter, // The meters to be processed by this profile
	addr: one AC_Ext_Ent // The address of the externel entity to send the processed data to
}{
	user.role = CONSUMER // Only consumers can have processing profiles
}
fact OneCurrentProcessingProfileForAllMeters {
	always all m:AC_Meter | one cpp:AC_Gateway_TSF.current_processing_profiles {
		m in cpp.meters
	}
}

var sig ProcessedMeterData extends Variable {
	var p_meter_data: one MeterData,
	var dest: one AC_Ext_Ent
}{
	// Fields should remain static through life
	(p_meter_data' = p_meter_data and dest' = dest) or after always this not in ProcessedMeterData
}

sig User extends Constant {
	name: Name,
	cred: Credentials,
	role: Role
}
sig Name extends Constant {}
enum Role { CONSUMER, TECH, ADMIN }

// ==================================================================
fact SystemInit {
	no Message
	// Components should limit their own state at model startup
}

fact MakeSystem {
	AtomicComponent = AC_Gateway_Comms + AC_Gateway_TSF + AC_Security_Module + AC_Meter + AC_Ext_Ent + AC_Admin
	CompositeComponent = System + CC_Gateway_TOE + CC_LMN + CC_WAN
	Connector = CON_LMN_NET + CON_TSF_Comms + CON_TSF_SecMod + CON_WAN_NET
	// Interface, operations and parameters
	Interface = IF_MTR2GW   + IF_GW2MTR +
				IF_SM2GW    + IF_GW2SM +
				IF_Comms2GW + IF_GW2Comms +
				IF_GW2ExtEnt + IF_ExtEnt2GW +
				IF_GW2Admin  + IF_Admin2GW
	Operation = OP_max_data_size + OP_process_mdata + OP_send_p_mdata +
				OP_send_raw_data + OP_auth_meter + OP_auth_meter_reply +
				OP_receive_p_data
	Parameter = P_RawMeterData   + P_MeterDataComms + 
				P_MeterAuth      + P_MeterAuth_reply +
				P_MeterAuth_data + P_ExtEntPData +
				P_ProcessedMeterData
	// Messages
	Message = OpCall
	// Value Types allowed in system
	Variable = MeterData + ProcessedMeterData
	Constant = Validity + Credentials + Name + User + ProcessingProfile
}


pred ex_0 {
	one m:AC_Meter | m.generateMeterData[]
}

pred ex_1 {
	//one m:AC_Meter | m.generateMeterData[] and after (one md:MeterData | m.sendMeterData[md])
	after one m:AC_Meter, md:MeterData | m.sendMeterData[md]
}
pred ex_1_1 {
	eventually some operation.OP_send_raw_data
}

pred ex_2 {
	eventually some operation.OP_process_mdata
}

pred ex_2_1 {
	eventually some oc:OpCall | oc.operation = OP_auth_meter and some received.oc
}

pred ex_3 {
	eventually OpCall.arguments[P_MeterAuth_reply] = VALID
}

pred ex_4 {
	eventually some pmd:ProcessedMeterData | AC_Gateway_TSF.sendProcessedMeterData[pmd]
	//some AC_Gateway_TSF.processed_meter_data
}

pred ex_5 {
	eventually some pmd:ProcessedMeterData | AC_Gateway_Comms.forwardProcessedMeterData[pmd]
}

pred ex_5_1 {
	eventually some oc:OpCall | oc.operation = OP_receive_p_data and some received.oc
}

pred ex_5_2 {
	eventually some p_mdata_storage
}

pred ex_6 {
	eventually some md:MeterData | AC_Gateway_TSF.processMeterData[md]
}

run ex_0 for 1 but 2 AC_Meter, 2 AC_Ext_Ent, 16 Component, 24 Port,
						16 Variable, 16 Constant, 2 Message,
						4 Int, 6 steps

//Temp property checks below here:




